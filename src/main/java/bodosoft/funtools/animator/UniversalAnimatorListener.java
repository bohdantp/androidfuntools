package bodosoft.funtools.animator;

import android.animation.Animator;
import android.animation.ValueAnimator;

/**
 * Created by bohdantp on 11/27/14.
 */
public abstract class UniversalAnimatorListener implements Animator.AnimatorListener, ValueAnimator.AnimatorUpdateListener {

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        onAnimationUpdate();
        onAnimationEnd();
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        onAnimationUpdate();
    }

    public abstract void onAnimationUpdate();

    public abstract void onAnimationEnd();
}
