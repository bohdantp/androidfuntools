package bodosoft.funtools.animator;

/**
 * Created by bohdantp on 11/27/14.
 */
public class ValueAnimator {
    private float progress;

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }
}
