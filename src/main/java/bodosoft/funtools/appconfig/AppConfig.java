package bodosoft.funtools.appconfig;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bohdantrofymchuk on 1/24/15.
 */
public class AppConfig {

    private static final String KEY_ACTUAL_APP_PACKAGE = "actualAppPackage";
    private static final String KEY_NEED_TO_MIGRATE = "needToMigrate";
    private static final String KEY_ACTUAL_APP_VERSION_CODE = "actualAppVersionCode";
    private String actualApppackage;
    private boolean needToMigrate;
    private int actualAppVersionCode;

    public static AppConfig parseJson(String json) throws JSONException {
        AppConfig res = null;
        JSONObject j = new JSONObject(json);
        res = new AppConfig();
        res.actualApppackage = j.optString(KEY_ACTUAL_APP_PACKAGE, "");
        res.needToMigrate = j.optBoolean(KEY_NEED_TO_MIGRATE, false);
        res.actualAppVersionCode = j.optInt(KEY_ACTUAL_APP_VERSION_CODE, 0);
        return res;
    }

    public String getActualApppackage() {
        return actualApppackage;
    }

    public boolean isNeedToMigrate() {
        return needToMigrate;
    }

    public int getActualAppVersionCode() {
        return actualAppVersionCode;
    }

}
