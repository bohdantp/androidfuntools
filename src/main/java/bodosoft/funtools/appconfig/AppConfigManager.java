package bodosoft.funtools.appconfig;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONException;

import bodosoft.funtools.utils.PrefUtil;

/**
 * Created by bohdantrofymchuk on 1/24/15.
 */
public class AppConfigManager {

    private static final String PREF_CAT = "appConfigMan";
    private static final String PREF_KEY = "currentappconf";

    public static void saveNewAppConfig(Context context, String newConfig) {
        PrefUtil.saveString(context, PREF_CAT, PREF_KEY, newConfig);
    }

    public static AppConfig getCurrentConfig(Context context) {
        AppConfig res = null;
        String json = PrefUtil.getString(context, PREF_CAT, PREF_KEY);
        if (!TextUtils.isEmpty(json)) {
            try {
                res = AppConfig.parseJson(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

}
