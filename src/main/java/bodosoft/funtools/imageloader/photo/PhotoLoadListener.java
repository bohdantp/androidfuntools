package bodosoft.funtools.imageloader.photo;

public interface PhotoLoadListener {

    void photoLoaded(ImageWrapper wrapper, boolean instantntly);

}
