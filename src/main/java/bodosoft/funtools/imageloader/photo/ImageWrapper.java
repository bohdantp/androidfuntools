package bodosoft.funtools.imageloader.photo;

import android.graphics.Bitmap;
import android.media.ExifInterface;

/**
 * Created by bohdantp on 07.02.14.
 */
public class ImageWrapper {

    public final Bitmap bitmap;
    public final ExifInterface info;

    public ImageWrapper(Bitmap bitmap, ExifInterface info) {
        this.bitmap = bitmap;
        this.info = info;
    }

}
