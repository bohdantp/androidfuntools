package bodosoft.funtools.imageloader.photo;

import android.graphics.Bitmap;
import androidx.collection.LruCache;

public class ImageCache {

    private LruCache<String, ImageWrapper> mMemoryCache;

    public ImageCache(int size) {

        mMemoryCache = new LruCache<String, ImageWrapper>(size) {

            protected int sizeOf(String key, ImageWrapper wrapper) {
                int res = 0;
                if (wrapper != null) {
                    Bitmap bitmap = wrapper.bitmap;
                    if (bitmap != null) {
                        res = bitmap.getWidth() * bitmap.getHeight() * 4;
                    }
                }
                return res;
            }

            @Override
            protected void entryRemoved(boolean evicted, String key, ImageWrapper oldWrapper, ImageWrapper newWrapper) {
                System.gc();
                super.entryRemoved(evicted, key, oldWrapper, newWrapper);
            }

        };
    }

    public void addBitmapToMemoryCache(String key, ImageWrapper wrapper) {
        if (getFromMemCache(key) == null) {
            mMemoryCache.put(key, wrapper);
        }
    }

    public ImageWrapper getFromMemCache(String key) {
        Object res = mMemoryCache.get(key);
        if (res != null) {
            return (ImageWrapper) res;
        }
        return null;
    }

    public void forceRemove(String key) {
        mMemoryCache.remove(key);
    }

    public void clearAll() {
        mMemoryCache.evictAll();
    }

}
