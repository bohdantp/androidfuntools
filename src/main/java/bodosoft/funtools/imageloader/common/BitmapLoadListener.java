package bodosoft.funtools.imageloader.common;

import android.graphics.Bitmap;

/**
 * Created by bohdantp on 07.10.2014.
 */
public interface BitmapLoadListener {
    void bitmapLoaded(Bitmap bitmap);
}
