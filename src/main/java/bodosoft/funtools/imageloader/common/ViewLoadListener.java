
package bodosoft.funtools.imageloader.common;

/**
 * Created by bohdantp on 19.09.13.
 */
public interface ViewLoadListener {

    void onLoadSuccess();

    void onLoadError(int errorCode, String errMsg);
}
