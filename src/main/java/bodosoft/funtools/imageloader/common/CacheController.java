package bodosoft.funtools.imageloader.common;

import java.io.File;

/**
 * Created by bohdantp on 13.09.13.
 */
public class CacheController {

    private final String path;

    public CacheController(String path) {
        this.path = path;
    }

    public synchronized String getPath(String key, String fileExtension) {
        String res = getRealPath(key, fileExtension);
        return res;
    }

    public String getRealPath(String key, String fileExtension) {
        return path + File.separator + "CH" + key.hashCode() + "." + fileExtension;
    }

    public synchronized boolean isCached(String key, String fileExtension) {
        File f = new File(getRealPath(key, fileExtension));
        return f.exists();
    }

    public synchronized void removeCacheFor(String key, String fileExtension) {
        File f = new File(getRealPath(key, fileExtension));
        f.delete();
    }

}
