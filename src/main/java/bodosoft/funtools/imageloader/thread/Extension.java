
package bodosoft.funtools.imageloader.thread;

import java.io.File;

public class Extension {

    private static final String APP_DIR = "ImgLoader";
    private static final String CACHE_FOLDER = "photo_cache";

    public static String getExternalAppDirPath() {
        return android.os.Environment.getExternalStorageDirectory() + File.separator + APP_DIR + File.separator;
    }

    public static String getCachePath() {
        return getExternalAppDirPath() + CACHE_FOLDER + File.separator;
    }

}
