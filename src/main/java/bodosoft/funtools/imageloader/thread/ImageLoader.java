package bodosoft.funtools.imageloader.thread;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import bodosoft.funtools.imageloader.common.BitmapLoadListener;
import bodosoft.funtools.imageloader.common.CacheController;
import bodosoft.funtools.imageloader.common.ViewLoadListener;
import bodosoft.funtools.imageloader.config.LoaderConfiguration;
import bodosoft.funtools.imageloader.photo.ImageCache;
import bodosoft.funtools.imageloader.photo.ImageWrapper;
import bodosoft.funtools.imageloader.photo.PhotoLoadListener;

public class ImageLoader {

    public static final int TAG_KEY = 1111111115;
    private static final String TAG = "ImageLoader";
    private static final String DEFAULT_FILE_EXTENSION = "png";
    private static final int LOADER_TAG = 1235123512;
    private static boolean DEBUG = false;
    private static ImageCache imageCache;
    private static ExecutorService uiloader = Executors.newFixedThreadPool(1);
    private Context context;
    private Resources resources;
    private Bitmap mLoadingBitmap;
    private Bitmap couldNotLoadBitmap;
    private LoaderConfiguration config;
    private Handler uiHandler;
    private CacheController cache;
    private ExecutorService webloadworker = Executors.newFixedThreadPool(2);
    private ExecutorService service = Executors.newFixedThreadPool(1);
    private Hashtable<String, ImageLoadTask> webTasks = new Hashtable<String, ImageLoadTask>();

    private ImageLoader() {
    }

    public static ImageLoader init(Context context, LoaderConfiguration config) {
        ImageLoader res = new ImageLoader();
        res.context = context;
        res.config = config;
        if (imageCache == null) {
            res.imageCache = new ImageCache(config.memoryCacheSize);
        }
        res.resources = context.getResources();
        res.mLoadingBitmap = BitmapFactory.decodeResource(res.resources, config.loadingImageResource);
        res.couldNotLoadBitmap = BitmapFactory.decodeResource(res.resources, config.couldnotloadImageResource);
        res.uiHandler = new Handler(context.getMainLooper());
        res.cache = new CacheController(config.cacheDirPath);
        return res;
    }

    private static boolean cancelPotentialWork(String data, ImageView imageView) {
        final PhotoLoader loader = getPhotoLoader(imageView);
        if (loader != null) {
            final String bitmapData = loader.getPath();
            if (!bitmapData.equals(data)) {
                if (DEBUG) {
                    Log.v(TAG, "cancelPotentialWork: cancel loader " + data);
                }
                loader.cancel();
            } else {
                if (loader.isWorked()) {
                    if (DEBUG) {
                        Log.v(TAG, "cancelPotentialWork: return false");
                    }
                    return false;
                }
            }
        }
        if (DEBUG) {
            Log.v(TAG, "cancelPotentialWork: return true");
        }
        return true;
    }

    private static PhotoLoader getPhotoLoader(ImageView imageView) {
        if (imageView != null) {
            Object o = imageView.getTag(LOADER_TAG);
            if (o != null) {
                PhotoLoader loader = (PhotoLoader) o;
                return loader;
            }
        }
        return null;
    }

    private static boolean verify(ImageView view, String path) {
        synchronized (view) {
            final PhotoLoader loader = getPhotoLoader(view);
            if (loader != null) {
                final String loaderPath = loader.getPath();
                if (loaderPath.equals(path)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void forceRemoveFromCache(String path) {
        cache.removeCacheFor(path, DEFAULT_FILE_EXTENSION);
        imageCache.forceRemove(getKeyByPath(path));
    }

    public void dispose() {

    }

    public void cancelForce(ImageView view) {
        synchronized (view) {
            cancelPotentialWork("", view);
        }
    }

    public boolean cancel(String path, ImageView view) {
        boolean res = false;
        synchronized (view) {
            final PhotoLoader loader = getPhotoLoader(view);
            if (loader != null) {
                final String bitmapData = loader.getPath();
                if (bitmapData.equals(path)) {
                    loader.cancel();
                    if (DEBUG) {
                        Log.v(TAG, "cancel: cancel load " + path);
                    }
                }
            } else {
                if (DEBUG) {
                    Log.v(TAG, "cancel: loader is null");
                }
            }
        }
        return res;
    }

    public String getTagValue(String path) {
        return path;
    }

    public void loadBitmapOnly(final String path, ImageView view, BitmapLoadListener bitmapLoadListener) {
        load(path, view, null, bitmapLoadListener);
    }

    public void load(final String path, ImageView view, ViewLoadListener viewLoadListener) {
        load(path, view, viewLoadListener, null);
    }

    /**
     * Use only from UI thread
     *
     * @param path
     * @param view
     * @param viewLoadListener
     */
    private void load(final String path, ImageView view, ViewLoadListener viewLoadListener, final BitmapLoadListener bitmapLoadListener) {
        load(path, view, viewLoadListener, bitmapLoadListener, DEFAULT_FILE_EXTENSION);
    }

    /**
     * Use only from UI thread
     *
     * @param path
     * @param view
     * @param viewLoadListener
     */
    private void load(final String path, ImageView view, ViewLoadListener viewLoadListener, final BitmapLoadListener bitmapLoadListener, String fileExtension) {
        if (path == null) {
            return;
        }
        synchronized (view) {
            if (cancelPotentialWork(path, view)) {
                final ImageWrapper b = imageCache.getFromMemCache(getKeyByPath(path));
                if (b != null) {
                    if (bitmapLoadListener != null) {
                        bitmapLoadListener.bitmapLoaded(b.bitmap);
                    } else {
                        Utils.setImageBitmap(view, b, true, resources, null, 250, getTagValue(path));
                        if (viewLoadListener != null) {
                            viewLoadListener.onLoadSuccess();
                        }
                    }
                    if (DEBUG) {
                        Log.v(TAG, "load: loaded from cache " + path);
                    }
                } else {
                    final WeakReference<ImageView> reference = new WeakReference<ImageView>(view);
                    final PhotoLoader loader = new PhotoLoader(new PhotoLoadListener() {

                        @Override
                        public void photoLoaded(final ImageWrapper wrapper, final boolean instantntly) {
                            uiHandler.post(new Runnable() {

                                @Override
                                public void run() {
                                    ImageView view = reference.get();
                                    if (view != null) {
                                        if (verify(view, path)) {
                                            if (bitmapLoadListener != null) {
                                                bitmapLoadListener.bitmapLoaded(wrapper.bitmap);
                                            } else {
                                                Utils.setImageBitmap(view, wrapper, !config.useTransition, resources, config.useLoadBitmapForTransition ? mLoadingBitmap
                                                        : null, config.transitionTime, getTagValue(path));
                                            }
                                            if (DEBUG) {
                                                Log.v(TAG, "load: loaded " + path);
                                            }


                                        }
                                    } else {
                                        if (DEBUG) {
                                            Log.v(TAG, "load: reference get null");
                                        }
                                    }
                                }
                            });
                        }
                    }, path, viewLoadListener, false, fileExtension);
                    if (bitmapLoadListener == null) {
                        final BitmapDrawable asyncDrawable = new BitmapDrawable(resources, mLoadingBitmap);
                        view.setImageDrawable(asyncDrawable);
                    }
                    view.setTag(LOADER_TAG, loader);
                    service.execute(loader);
                }
            }
        }
    }

    public void loadWebOnly(String path, ImageView view, ViewLoadListener viewLoadListener) {
        loadWebOnly(path, view, viewLoadListener, DEFAULT_FILE_EXTENSION);
    }

    public void loadWebOnly(String path, ImageView view, ViewLoadListener viewLoadListener, String fileExtension) {
        synchronized (view) {
            if (cancelPotentialWork(path, view)) {
                final ImageWrapper b = imageCache.getFromMemCache(getKeyByPath(path));
                if (b != null) {
                    viewLoadListener.onLoadSuccess();
                } else {
                    PhotoLoader loader = new PhotoLoader(null, path, viewLoadListener, true, fileExtension);
                    view.setTag(LOADER_TAG, loader);
                    service.execute(loader);
                }
            }
        }
    }

    public void loadFileFromWebOnly(final String requestedPath, final ImageLoadListener listener) {
        loadFileFromWebOnly(requestedPath, listener, "jpg");
    }

    public void loadFileFromWebOnly(final String requestedPath, final ImageLoadListener listener, String fileExtension) {
        ImageLoadTask t = webTasks.get(requestedPath);
        if (t == null) {
            if (cache.isCached(requestedPath, fileExtension)) {
                listener.imageLoaded(cache.getPath(requestedPath, fileExtension));
            } else {
                ImageLoadTask webTask = new ImageLoadTask(requestedPath, cache.getPath(requestedPath, fileExtension), new ImageLoadListener() {
                    @Override
                    public void webWorkIsDone() {
                        webTasks.remove(requestedPath);
                    }

                    @Override
                    public void imageLoaded(String path) {
                        listener.imageLoaded(path);
                    }

                    @Override
                    public void imageLoadError(int errorCode, String message) {
                        listener.imageLoadError(errorCode, message);
                    }
                }, config.connectionTimeOut);
                webTasks.put(requestedPath, webTask);
                webloadworker.execute(webTask);
            }
        } else {
            t.addListener(listener);
        }
    }

    public synchronized ImageWrapper loadPhoto(String path) {

        ExifInterface exif = null; //Since API Level 5
        try {
            exif = new ExifInterface(path);
            String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            //            Log.v(TAG, "exifOrientation " + exifOrientation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Matrix matrix = Utils.getMatrix(exif);

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        if (config.thumbnailWidth != -1) {
            final int targetW = config.thumbnailWidth;
            final int targetH = config.thumbnailHeight;
            final int photoW = bmOptions.outWidth;
            final int photoH = bmOptions.outHeight;
            float maxScale = Math.max((float) photoW / (float) targetW, (float) photoH / (float) targetH);
            double log = Math.log(maxScale) / Math.log(2);
            long round = Math.round(log);
            int s = (int) Math.pow(2, round);
            bmOptions.inSampleSize = s > 1 ? s : 1;
        }
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPreferredConfig = config.config;
        final Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        if (bitmap == null) {
            new File(path).delete();
            return new ImageWrapper(null, null);
        }

        if (matrix != null) {
            Bitmap b = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
            bitmap.recycle();
            //            if (b == null) {
            //                Log.v("JJJ", "b " + path);
            //            }
            return new ImageWrapper(b, exif);
        } else {
            //            if (bitmap == null) {
            //                Log.v("JJJ", "bitmap " + path);
            //            }
            return new ImageWrapper(bitmap, exif);
        }

    }

    public synchronized Bitmap loadVideoPreview(String path) {
        return getVideoThumbnail(context, path);
    }

    private Bitmap getVideoThumbnail(Context appContext, String videoPath) {
        Bitmap res;
        final long fileId = getVideoIdByPath(appContext, videoPath);
        final String[] projection = new String[]{
                MediaStore.MediaColumns.DATA
        };
        final String selection = MediaStore.Video.Thumbnails.VIDEO_ID + " = ? ";
        final String[] selectionArgs = new String[]{
                String.valueOf(fileId)
        };
        final Cursor thumbCursor = appContext.getContentResolver().
                query(
                        MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                        projection,
                        selection,
                        selectionArgs,
                        null);

        if (thumbCursor != null && thumbCursor.moveToFirst()) {
            // the requestedPath is stored in the DATA column
            int dataIndex = thumbCursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            res = loadPhoto(thumbCursor.getString(dataIndex)).bitmap;
        } else {
            res = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
        }
        if (thumbCursor != null) {
            thumbCursor.close();
        }
        return res;
    }

    private long getVideoIdByPath(Context appContext, String path) {
        long res = 0;
        final String[] projectionVideo = {
                MediaStore.Video.Media._ID
        };
        final String selection = MediaStore.Video.Media.DATA + "=?";
        final String[] selectionArgs = new String[]{
                path
        };
        Cursor videoCursor = appContext.getContentResolver()
                .query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projectionVideo, selection, selectionArgs, null);
        if (videoCursor != null && videoCursor.moveToFirst()) {
            res = videoCursor.getLong(videoCursor.getColumnIndex(MediaStore.Video.Media._ID));
        }
        if (videoCursor != null) {
            videoCursor.close();
        }
        return res;
    }

    private String getKeyByPath(String originalPath) {
        return originalPath + "" + config.thumbnailWidth + "" + config.thumbnailHeight;
    }

    public String getRealPath(String url, String fileExtension) {
        return cache.getRealPath(url, fileExtension);
    }

    private class PhotoLoader implements Runnable, ImageLoadListener {

        private final PhotoLoadListener listener;
        private final String requestedPath;
        private final ViewLoadListener viewLoadListener;
        private final boolean fromwebonly;
        private volatile boolean work = true;
        private UILoadTask uitask;
        private String fileExtension;

        public PhotoLoader(PhotoLoadListener listener, String requestedPath, ViewLoadListener viewLoadListener, boolean fromwebonly, String fileExtension) {
            this.listener = listener;
            this.requestedPath = requestedPath;
            this.viewLoadListener = viewLoadListener;
            this.fromwebonly = fromwebonly;
            this.fileExtension = fileExtension;
        }

        public void cancel() {
            work = false;
            ImageLoadTask webTask = webTasks.get(requestedPath);
            if (webTask != null) {
                webTask.cancel(this);
            }
        }

//        public PhotoLoader(PhotoLoadListener listener, String requestedPath, ViewLoadListener viewLoadListener, boolean fromwebonly) {
//            this(listener, requestedPath, viewLoadListener, fromwebonly, DEFAULT_FILE_EXTENSION);
//        }

        public boolean isWorked() {
            return work;
        }

        public String getPath() {
            return requestedPath;
        }

        private void loadLocalPath(String localPath, String originalPath) {
            if (!work) {
                return;
            }
            Bitmap res;
            ImageWrapper wrapper = null;
            final String mimeType = Utils.getMimeTypeFromPath(localPath);
            if (!TextUtils.isEmpty(mimeType) && mimeType.toLowerCase().contains("video")) {
                res = loadVideoPreview(localPath);
                wrapper = new ImageWrapper(res, null);
            } else {
                wrapper = loadPhoto(localPath);
            }
            if (work && wrapper != null && localPath != null) {
                imageCache.addBitmapToMemoryCache(getKeyByPath(originalPath), wrapper);
                if (work) {
                    if (listener != null) {
                        listener.photoLoaded(wrapper, false);
                    }
                    if (viewLoadListener != null) {
                        viewLoadListener.onLoadSuccess();
                    }
                }
            }
            work = false;
        }

        private void loadLocal(String s1, String s2) {
            uitask = new UILoadTask(s1, s2);
            uiloader.execute(uitask);
        }

        @Override
        public void webWorkIsDone() {
            webTasks.remove(requestedPath);
        }

        @Override
        public void imageLoaded(String path) {
            loadLocal(path, requestedPath);
        }

        @Override
        public void imageLoadError(int errorCode, String message) {
            if (couldNotLoadBitmap != null) {
                if (listener != null) {
                    listener.photoLoaded(new ImageWrapper(couldNotLoadBitmap, null), false);
                }
                if (viewLoadListener != null) {
                    viewLoadListener.onLoadError(errorCode, message);
                }
            }
            work = false;
        }

        public void run() {
            if (!work) {
                return;
            }
            if (requestedPath.startsWith("http")) {
                ImageLoadTask t = webTasks.get(requestedPath);
                if (t == null) {
                    if (cache.isCached(requestedPath, fileExtension)) {
                        loadLocal(cache.getPath(requestedPath, fileExtension), requestedPath);
                    } else {
                        ImageLoadTask webTask = new ImageLoadTask(requestedPath, cache.getPath(requestedPath, fileExtension), this, config.connectionTimeOut);
                        webTasks.put(requestedPath, webTask);
                        webloadworker.execute(webTask);
                    }
                } else {
                    t.addListener(this);
                }
            } else {
                loadLocal(requestedPath, requestedPath);
            }
        }

        private class UILoadTask implements Runnable {
            private final String localPath;
            private final String originalPath;

            private UILoadTask(String localPath, String originalPath) {
                this.localPath = localPath;
                this.originalPath = originalPath;
            }

            @Override
            public void run() {
                if (!fromwebonly) {
                    loadLocalPath(localPath, originalPath);
                } else {
                    viewLoadListener.onLoadSuccess();
                }
                work = false;
            }

        }
    }

}
