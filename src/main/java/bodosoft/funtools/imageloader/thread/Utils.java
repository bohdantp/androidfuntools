
package bodosoft.funtools.imageloader.thread;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.ExifInterface;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import bodosoft.funtools.imageloader.photo.ImageWrapper;

public class Utils {

    private static final String TAG = "Utils";

    public static void setImageBitmap(ImageView imageView, ImageWrapper wrapper, boolean instanty, Resources rec, Bitmap transitionBitmap, int transitionTime, String tagValue) {
//        Log.v(TAG, "set image: " + wrapper.bitmap.getWidth() + " X " + wrapper.bitmap.getHeight());
        if (!instanty) {
            final TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                    transitionBitmap == null ?
                            new ColorDrawable(android.R.color.transparent) : new BitmapDrawable(rec, transitionBitmap), new BitmapDrawable(rec, wrapper.bitmap)
            });
            imageView.setImageDrawable(td);
            td.startTransition(transitionTime);
        } else {
            imageView.setImageBitmap(wrapper.bitmap);
        }
        imageView.setTag(ImageLoader.TAG_KEY, tagValue);
    }

    public static Matrix getMatrix(ExifInterface info) {
        Matrix matrix = new Matrix();
        ExifInterface exifReader = info;
        if (exifReader == null) {
            return matrix;
        }
        int orientation = exifReader.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                matrix = null;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(270);
                break;
            default:
                matrix = null;
        }
        return matrix;
    }

    public static String getMimeTypeFromURL(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static String getMimeTypeFromPath(String path) {
        String type = null;
        int ind = path.lastIndexOf('.') + 1;
        if (ind > 0 && ind < path.length()) {
            String ext = path.substring(ind);
            String myme = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext.toLowerCase());
            type = myme;
        }
        return type;
    }
}
