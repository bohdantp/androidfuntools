package bodosoft.funtools.imageloader.thread;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

public class ImageLoadTask implements Runnable {

    public static final int BASIC_ERROR_CODE = 0;
    public static final int FILE_NOT_FOUNT_ON_SERVER_ERROR_CODE = 1;
    public static final int BUFFER_SIZE = 8096;
    private final String source;
    private final int connectionTimeOut;
    private String destPath;
    private Vector<ImageLoadListener> listeners = new Vector<ImageLoadListener>();
    private InputStream input = null;
    private OutputStream output = null;
    private boolean work = true;

    public ImageLoadTask(String source, String destPath, ImageLoadListener listener, int connectionTimeOut) {
        this.source = source;
        this.destPath = destPath;
        this.listeners.add(listener);
        this.connectionTimeOut = connectionTimeOut;
    }

    public void addListener(ImageLoadListener listener) {
        listeners.add(listener);
    }

    public void run() {
        if (!work) {
            return;
        }
        //        long start = System.currentTimeMillis();
        try {
            final URL url = new URL(source);
            //            final HttpURLConnection connection = client.open(url);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(connectionTimeOut);
            connection.setReadTimeout(connectionTimeOut);
            connection.connect();
            File f = new File(destPath);
            if (!f.getParentFile().exists()) {
                f.getParentFile().mkdirs();
            }

            input = connection.getInputStream();
            output = new FileOutputStream(destPath);
            byte data[] = new byte[BUFFER_SIZE];
            int all = 0;
            int count;
            //            Log.v("yyy", "start read");
            while ((count = input.read(data)) != -1) {
                all += count;
                if (!work) {
                    closeStreams();
                    removeFile();
                    return;
                }
                output.write(data, 0, count);
            }
            output.flush();
            //            Log.v("yyy", "end read");
            if (all < 5) {
                error();
                Log.v("nn", "error 0 bytes");
                for (ImageLoadListener listener : listeners) {
                    listener.imageLoadError(BASIC_ERROR_CODE, "could not load image 0 bytes loaded: " + source);
                }

                return;
            }
            closeStreams();
            if (work) {
                for (ImageLoadListener listener : listeners) {
                    listener.webWorkIsDone();
                    listener.imageLoaded(destPath);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            if (work) {
                for (ImageLoadListener listener : listeners) {
                    listener.webWorkIsDone();
                    listener.imageLoadError(FILE_NOT_FOUNT_ON_SERVER_ERROR_CODE, "could not load image: " + source);
                }
            }
            closeStreams();
            removeFile();
        } catch (Exception e) {
            e.printStackTrace();

            closeStreams();
            removeFile();
            if (work) {
                for (ImageLoadListener listener : listeners) {
                    listener.webWorkIsDone();
                    listener.imageLoadError(BASIC_ERROR_CODE, "could not load image: " + source);
                }
            }
        }

        //        long end = System.currentTimeMillis();
        //        Log.v("TTT", "time " + (end - start));

    }

    private void removeFile() {
        File f = new File(destPath);
        f.delete();
    }

    private void error() {
        work = false;
        closeStreams();
        removeFile();
    }

    private void closeStreams() {
        if (input != null) {
            try {
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (output != null) {
            try {
                output.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void cancel(ImageLoadListener listener) {
        if (listeners.size() == 1) {
            listeners.get(0).webWorkIsDone();
        }
        listeners.remove(listener);
        if (listeners.size() < 1) {
            work = false;
        }
    }

}
