
package bodosoft.funtools.imageloader.thread;

public interface ImageLoadListener {
    void webWorkIsDone();

    void imageLoaded(String path);

    void imageLoadError(int errorCode, String message);
}
