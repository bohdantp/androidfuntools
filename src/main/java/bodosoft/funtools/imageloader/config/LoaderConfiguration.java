package bodosoft.funtools.imageloader.config;

import android.graphics.Bitmap.Config;

import bodosoft.funtools.R;

public class LoaderConfiguration {

    public final int connectionTimeOut;
    public final int thumbnailWidth;
    public final int thumbnailHeight;
    public final int memoryCacheSize; // size in mbytes
    public final int loadingImageResource;
    public final int couldnotloadImageResource;
    public final Config config;
    public final String cacheDirPath;

    public boolean useLoadBitmapForTransition = true;
    public boolean useTransition = true;
    public int transitionTime = 350;

    private LoaderConfiguration(Builder builder) {
        thumbnailWidth = builder.thumbnailWidth;
        thumbnailHeight = builder.thumbnailHeight;
        connectionTimeOut = builder.connectionTimeOut;
        memoryCacheSize = builder.memoryCacheSize;
        loadingImageResource = builder.loadingImageResource;
        config = builder.config;
        cacheDirPath = builder.cacheDirPath;
        couldnotloadImageResource = builder.couldnotloadImageResource;
        useLoadBitmapForTransition = builder.useLoadBitmapForTransition;
        useTransition = builder.useTransition;
        transitionTime = builder.transitionTime;
    }

    public static class Builder {
        private int thumbnailWidth = -1;
        private int thumbnailHeight = -1;
        private int connectionTimeOut = 10000;
        private int memoryCacheSize = 2 * 1024 * 1024; // size in bytes
        private int loadingImageResource = R.drawable.ic_launcher;
        private int couldnotloadImageResource = R.drawable.ic_launcher;
        private Config config = Config.ARGB_8888;
        private boolean useLoadBitmapForTransition = true;
        private boolean useTransition = true;
        private int transitionTime = 350;
        private String cacheDirPath = "";

        public Builder() {
        }

        public Builder setCacheDirPath(String cacheDirPath) {
            this.cacheDirPath = cacheDirPath;
            return Builder.this;
        }

        public LoaderConfiguration build() {
            return new LoaderConfiguration(this);
        }

        public Builder setThumbnailSize(int w, int h) {
            thumbnailWidth = w;
            thumbnailHeight = h;
            return Builder.this;
        }

        public Builder setTransitionTime(int val) {
            transitionTime = val;
            return Builder.this;
        }

        public Builder setUseLoadBitmapForTransition(boolean val) {
            useLoadBitmapForTransition = val;
            return Builder.this;
        }

        public Builder setUseTransition(boolean val) {
            useTransition = val;
            return Builder.this;
        }

        public Builder setConnectionTimeOut(int value) {
            connectionTimeOut = value;
            return Builder.this;
        }

        public Builder setConfig(Config config) {
            this.config = config;
            return Builder.this;
        }

        public Builder setMemoryCacheSize(int size) {
            memoryCacheSize = size;
            return Builder.this;
        }

        public Builder setLoadingImageResource(int res) {
            loadingImageResource = res;
            return Builder.this;
        }

        public Builder setCouldNotLoadImageResource(int res) {
            couldnotloadImageResource = res;
            return Builder.this;
        }

    }

}
