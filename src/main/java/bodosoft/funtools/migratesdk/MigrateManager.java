package bodosoft.funtools.migratesdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import bodosoft.funtools.R;
import bodosoft.funtools.appconfig.AppConfig;
import bodosoft.funtools.appconfig.AppConfigManager;
import bodosoft.funtools.utils.MyMarketUtils;

/**
 * Created by bohdantrofymchuk on 1/24/15.
 */
public class MigrateManager {
    private static final String TAG = "MigrateManager";

    public static boolean checkMigrate(final Activity activity) {
        final AppConfig appConfig = AppConfigManager.getCurrentConfig(activity);
        if (appConfig == null) {
            return false;
        }
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            int actualAppVersionCode = appConfig.getActualAppVersionCode();
            if (actualAppVersionCode > pInfo.versionCode) {
                Log.v(TAG, "your version code is " + pInfo.versionCode + ", but actual is " +
                        actualAppVersionCode + " Please update your app");
            }
            if (appConfig.isNeedToMigrate()) {
                final String actualApppackage = appConfig.getActualApppackage();
                if (!pInfo.packageName.equals(actualApppackage)) {
                    boolean exists = false;
                    try {
                        activity.getPackageManager().getPackageInfo(actualApppackage, 0);
                        exists = true;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    Intent intent = activity.getPackageManager().getLaunchIntentForPackage(actualApppackage);
                    if (exists && intent != null) {
                        activity.startActivity(intent);
                        activity.finish();
                    } else {
                        View group = activity.getLayoutInflater().inflate(R.layout.migrate_your_app_dialog, null);
                        AlertDialog.Builder res = new AlertDialog.Builder(activity)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String myUrl = MyMarketUtils.getMarketURI(actualApppackage);
                                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(myUrl));
                                        activity.startActivity(i);
                                        activity.finish();
                                    }
                                })
                                .setNegativeButton(activity.getString(R.string.cancel),
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                activity.finish();
                                            }
                                        }
                                ).setTitle(activity.getString(R.string.warning))
                                .setView(group).setCancelable(false);
                        res.create().show();
                    }
                }
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

}
