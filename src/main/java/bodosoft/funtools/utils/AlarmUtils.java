package bodosoft.funtools.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

/**
 * Created by bohdantp on 29.09.2014.
 */
public class AlarmUtils {

    public static void sceduleAlarm(Context context, Class serviceclass, long firsStartAfter, long period) {
        Intent myIntent = new Intent(context, serviceclass);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, myIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, (int) firsStartAfter / 1000);
        // alarmManager.set(AlarmManager.RTC_WAKEUP,
        // calendar.getTimeInMillis(), pendingIntent);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), period, pendingIntent);

    }


}
