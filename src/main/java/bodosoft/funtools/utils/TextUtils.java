package bodosoft.funtools.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bohdantrofymchuk on 4/5/14.
 */
public class TextUtils {

    public static String inputStreamToString(InputStream is) {
        String line = "";
        StringBuilder total = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        try {
            while ((line = rd.readLine()) != null) {
                total.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return total.toString();
    }

    public static List<String> inputStreamToStringList(InputStream is) {
        List<String> res = new ArrayList<String>();
        String line = "";
        BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        try {
            while ((line = rd.readLine()) != null) {
                res.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }



    public static String formatTimeToMinutesText(long seconds) {
        return String.valueOf(seconds / 60) + ":" + (seconds % 60 < 10 ? "0" : "") + String.valueOf(seconds % 60);
    }

}
