package bodosoft.funtools.utils;

/**
 * Created by bohdantp on 11/27/14.
 */
public class MapUtils {

    public static float map(float value, float realRangeStart, float realRangeEnd, float neededRangeStart, float neededRangeEnd) {
        value = filterValue(value, realRangeStart, realRangeEnd);
        float res = (((value - realRangeStart) / (realRangeEnd - realRangeStart)) * (neededRangeEnd - neededRangeStart)) + neededRangeStart;
        return res;
    }

    private static float filterValue(float value, float realRangeStart, float realRangeEnd) {
        float res = value;
        if (realRangeStart > realRangeEnd) {
            if (res > realRangeStart) {
                res = realRangeStart;
            }
            if (res < realRangeEnd) {
                res = realRangeEnd;
            }
        } else {
            if (res < realRangeStart) {
                res = realRangeStart;
            }
            if (res > realRangeEnd) {
                res = realRangeEnd;
            }
        }
        return res;
    }
}
