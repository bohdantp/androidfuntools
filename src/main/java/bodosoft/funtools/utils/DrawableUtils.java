package bodosoft.funtools.utils;

import android.graphics.Rect;
import android.util.Log;

/**
 * Created by bohdantp on 09.10.2014.
 */
public class DrawableUtils {

    private static final String TAG = "DrawableUtils";

    public static Rect mapDrawableCenterCrop(float targetWidth, float targetHeight, float w, float h) {
        float scale = Math.max(targetWidth / w, targetHeight / h);
        Rect r1 = new Rect(0, 0, (int) targetWidth, (int) targetHeight);
        float cx = r1.exactCenterX();
        float cy = r1.exactCenterY();
        float startx = cx - w * scale / 2;
        float starty = cy - h * scale / 2;
        Rect result = new Rect((int) startx, (int) starty, (int) (startx + w * scale), (int) (starty + h * scale));
        Log.v(TAG, "res:" + result);
        return result;
    }
}
