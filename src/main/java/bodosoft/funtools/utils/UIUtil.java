package bodosoft.funtools.utils;

import android.os.Looper;

/**
 * Created by bohdantp on 08.10.2014.
 */
public class UIUtil {

    public static boolean isInUIThread() {
        boolean b = Looper.myLooper() == Looper.getMainLooper();
        return b;
    }

    public static void crashIfInUI() {
        if (isInUIThread()) {
            throw new IllegalStateException("You can't use this in ui thread");
        }
    }

}
