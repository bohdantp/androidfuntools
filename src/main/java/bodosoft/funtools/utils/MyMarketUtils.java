package bodosoft.funtools.utils;

import android.content.Context;
import android.util.Log;

/**
 * Created by bohdantp on 1/9/15.
 */
public class MyMarketUtils {
    private static final String TAG = "MarketUtils";

    public static String getMarketURI(Context context) {
        String res = "market://details?id=" + context.getPackageName();
        Log.v(TAG, "res:" + res);
        return res;
    }


    public static String getMarketURI(String packageName) {
        String res = "market://details?id=" + packageName;
        Log.v(TAG, "res:" + res);
        return res;
    }

    public static String getMarketUrl(Context context) {
        String res = "https://play.google.com/store/apps/details?id=" + context.getPackageName();
        Log.v(TAG, "res:" + res);
        return res;
    }

}
