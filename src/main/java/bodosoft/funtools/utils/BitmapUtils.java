package bodosoft.funtools.utils;

import android.graphics.Bitmap;

/**
 * Created by bohdantp on 5/15/15.
 */
public class BitmapUtils {

    public static Bitmap createScaledBitmap(Bitmap src, int maxWidth, int maxHeight) {
        float kx = ((float) src.getWidth()) / maxWidth;
        float ky = ((float) src.getHeight()) / maxHeight;
        float scale = Math.max(kx, ky);
        scale = Math.max(scale, 1);
        int w = (int) (src.getWidth() / scale);
        int h = (int) (src.getHeight() / scale);
        final Bitmap scaledBitmap = Bitmap.createScaledBitmap(src, w, h, false);
        return scaledBitmap;
    }

}
