package bodosoft.funtools.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by bohdantp on 10.09.2014.
 */
public class AddFreeUtil {

    public static boolean isAddFree(Context context, String appPackage, String addFreePackage) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo i1 = packageManager.getPackageInfo(appPackage, PackageManager.GET_SIGNATURES);
            PackageInfo i2 = packageManager.getPackageInfo(addFreePackage, PackageManager.GET_SIGNATURES);
            if (i1.signatures == null || i1.signatures.length < 1) {
                return false;
            }
            if (i2.signatures == null || i2.signatures.length < 1) {
                return false;
            }
            boolean res = i1.signatures[0].toCharsString().equals(i2.signatures[0].toCharsString());
            return res;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean isAddFreeSimple(Context context, String addFreePackage) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo i2 = packageManager.getPackageInfo(addFreePackage, PackageManager.GET_SIGNATURES);
            if (i2.signatures == null) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
