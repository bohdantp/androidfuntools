package bodosoft.funtools.utils;

import android.view.View;

/**
 * Created by bohdantp on 5/18/15.
 */
public class TagExtractor<P> {

    public P extract(View v) {
        if (v != null) {
            Object tag = v.getTag();
            if (tag != null) {
                try {
                    return (P) tag;
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
