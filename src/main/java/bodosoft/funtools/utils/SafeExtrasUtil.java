package bodosoft.funtools.utils;

import android.os.Bundle;

import java.util.HashSet;

/**
 * Created by bohdantp on 1/30/15.
 */
public class SafeExtrasUtil {

    public static final String POSTFIX = "_hash_val";
    public static HashSet<String> set = new HashSet<String>();

    public static void putExtra(Bundle b, String key, Bundle val) {
        String hash = String.valueOf(key.hashCode()) + String.valueOf(System.currentTimeMillis());
        val.putString(getKey(key), hash);
        b.putBundle(key, val);
    }

    private static String getKey(String key) {
        return key + POSTFIX;
    }

    public static Bundle getExtra(Bundle b, String key) {
        Bundle res = b.getBundle(key);
        if (res == null) {
            return null;
        }
        String hash = res.getString(getKey(key));
        if (android.text.TextUtils.isEmpty(hash)) {
            return null;
        }
        if (set.contains(hash)) {
            return null;
        }
        set.add(hash);
        return b.getBundle(key);

    }

}
