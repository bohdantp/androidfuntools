package bodosoft.funtools.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by bohdantrofymchuk on 9/30/14.
 */
public class FileUtils {

    public static void cleanFolder(File folder, File notClean) {
        if (folder.exists()) {
            File[] files = folder.listFiles();
            for (File file : files) {
                if (notClean == null || !file.getAbsolutePath().equals(notClean.getAbsolutePath())) {
                    file.delete();
                }
            }
        }
    }

    public static void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    public static void setNoMediaDir(File dir) {
        File output = new File(dir.getAbsolutePath() + File.separator + ".nomedia/");
        output.mkdirs();
    }

}
