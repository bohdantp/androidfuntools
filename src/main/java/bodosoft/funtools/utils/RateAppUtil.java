package bodosoft.funtools.utils;

import android.content.Context;

/**
 * Created by bohdantrofymchuk on 7/13/14.
 */
public class RateAppUtil {

    private static final String ASKED_KEY = "alreadyask";
    private static String PREF_CATEGORY = "asskRate";
    private final Context context;
    private final String[] keys;
    private final boolean alreadyAsked;

    public RateAppUtil(Context context, String... keys) {
        this.context = context;
        this.keys = keys;
        alreadyAsked = PrefUtil.getBoolean(context, PREF_CATEGORY, ASKED_KEY, false);
    }

    public void setOption(String key) {
        if (alreadyAsked) return;

        for (int i = 0; i < keys.length; i++) {
            if (keys[i].equals(key)) {
                PrefUtil.saveBoolean(context, PREF_CATEGORY, key, true);
                return;
            }
        }
        throw new IllegalArgumentException("wrong key");
    }

    public boolean askIfNeeded(Listener listener) {
        if (alreadyAsked) return false;
        if (!PrefUtil.getBoolean(context, PREF_CATEGORY, ASKED_KEY, false)) {
            for (int i = 0; i < keys.length; i++) {
                if (!PrefUtil.getBoolean(context, PREF_CATEGORY, keys[i], false)) {
                    return false;
                }
            }
            listener.showDialog();
            return true;
        }
        return false;
    }

    public void dontAskAnyMore() {
        PrefUtil.saveBoolean(context, PREF_CATEGORY, ASKED_KEY, true);
    }

    public interface Listener {
        void showDialog();
    }
}
