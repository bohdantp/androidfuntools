package bodosoft.funtools.thread;

import android.os.AsyncTask;

/**
 * Created by bohdantrofymchuk on 4/5/14.
 */
public class AsyncUtil {

    public static AsyncTask executeAsync(Runnable runnable) {
        AsyncRun asyncRun = new AsyncRun(runnable);
        asyncRun.execute();
        return asyncRun;
    }

    private static class AsyncRun extends AsyncTask<Void, Void, Void> {

        private Runnable runnable;

        private AsyncRun(Runnable runnable) {
            this.runnable = runnable;
        }

        @Override
        protected Void doInBackground(Void... params) {
            runnable.run();
            return null;
        }
    }
}
