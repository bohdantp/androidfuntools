package bodosoft.funtools.admob;

import android.content.Context;

/**
 * Created by bohdantrofymchuk on 1/30/15.
 */
public class InterstitialHandler {

    private final int coolDownTime = 60 * 1000;
    int slideMenuCount = 0;
    int itemClickCount = 0;
    private boolean shown = false;
    private AdsConfig adsConfig;
    private long lastShowTime = 0;

    public InterstitialHandler(Context context) {
        adsConfig = AdsConfig.getCurrentConfig(context);
    }

    public void setShown() {
        shown = true;
    }

    public void reloaded() {
        shown = false;
        slideMenuCount = 0;
        itemClickCount = 0;

    }

    private Response showInter() {
        long time = System.currentTimeMillis();
        if (time - lastShowTime > coolDownTime) {
            lastShowTime = time;
            return Response.SHOW;
        }
        return Response.DONT_SHOW;
    }

    public Response wantShowOnSlideMenu() {
        slideMenuCount++;
        if (slideMenuCount >= adsConfig.getInterstitialSlideMenuCountToShow()) {
            if (shown) {
                if (adsConfig.isInterstitialCanRepeat()) {
                    return Response.RELOAD;
                } else {
                    return Response.DONT_SHOW;
                }
            } else {
                return showInter();
            }
        }
        return Response.DONT_SHOW;
    }

    public Response wantShowOnAudioClick() {
        itemClickCount++;
        int interstitialAudioClickCount = adsConfig.getInterstitialAudioClickCount();
        if (interstitialAudioClickCount > 0 && itemClickCount >= interstitialAudioClickCount) {
            if (shown) {
                if (adsConfig.isInterstitialCanRepeat()) {
                    return Response.RELOAD;
                } else {
                    return Response.DONT_SHOW;
                }
            } else {
                return showInter();
            }
        }
        return Response.DONT_SHOW;
    }


    public enum Response {
        SHOW, RELOAD, DONT_SHOW;
    }

}
