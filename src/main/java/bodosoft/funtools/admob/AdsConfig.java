package bodosoft.funtools.admob;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import bodosoft.funtools.utils.PrefUtil;

/**
 * Created by bohdantrofymchuk on 2/7/15.
 */
public class AdsConfig {

    public static final String JSON_KEY_INTERSTITIAL_SLIDEMENU_COUNT = "interstitialSlideMenuCountToShow";
    public static final String JSON_KEY_INTERSTITIAL_CAN_REPEAT = "interstitialCanRepeatInOneSession";
    public static final String JSON_KEY_INTERSTITIAL_AUDIO_CLICK_COUNT = "interstitialOnAudioItemClickCount";
    private static final String PREF_CAT = "adsconfigcat";
    private static final String PREF_KEY = "adsconfkey";
    private int interstitialAudioClickCount = 3;
    private int interstitialSlideMenuCountToShow = 2;
    private boolean interstitialCanRepeat = false;

    private AdsConfig() {
    }

    public static void saveNewAdsConfig(Context context, String newConfig) {
        PrefUtil.saveString(context, PREF_CAT, PREF_KEY, newConfig);
    }

    public static AdsConfig getCurrentConfig(Context context) {
        AdsConfig res = new AdsConfig();
        String json = PrefUtil.getString(context, PREF_CAT, PREF_KEY);
        if (!TextUtils.isEmpty(json)) {
            try {
                JSONObject j = new JSONObject(json);
                res.interstitialSlideMenuCountToShow = j.optInt(JSON_KEY_INTERSTITIAL_SLIDEMENU_COUNT, res.interstitialSlideMenuCountToShow);
                res.interstitialAudioClickCount = j.optInt(JSON_KEY_INTERSTITIAL_AUDIO_CLICK_COUNT, res.interstitialAudioClickCount);
                res.interstitialCanRepeat = j.optBoolean(JSON_KEY_INTERSTITIAL_CAN_REPEAT, res.interstitialCanRepeat);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public int getInterstitialAudioClickCount() {
        return interstitialAudioClickCount;
    }

    public boolean isInterstitialCanRepeat() {
        return interstitialCanRepeat;
    }

    public int getInterstitialSlideMenuCountToShow() {
        return interstitialSlideMenuCountToShow;
    }
}
