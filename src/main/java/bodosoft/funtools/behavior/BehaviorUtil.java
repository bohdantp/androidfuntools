package bodosoft.funtools.behavior;

import android.content.Context;

import bodosoft.funtools.utils.PrefUtil;

/**
 * Created by bohdantp on 1/30/15.
 */
public class BehaviorUtil {

    private static final String PREF_CAT = "funtools_behaviuor_table";
    private static final String KEY_LAST_ACTIVE_ACTION = "last_active";

    public static void logActiveAction(Context context) {
        PrefUtil.saveLong(context, PREF_CAT, KEY_LAST_ACTIVE_ACTION, System.currentTimeMillis());
    }


    public static long getLastLaunchTime(Context context) {
        return PrefUtil.getLong(context, PREF_CAT, KEY_LAST_ACTIVE_ACTION, 0l);
    }

}
