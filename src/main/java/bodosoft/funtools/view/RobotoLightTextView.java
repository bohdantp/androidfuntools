package bodosoft.funtools.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by oscar on 07.07.14.
 */
public class RobotoLightTextView extends TextView {
    public RobotoLightTextView(Context context) {
        super(context);
        setRobotoTypeface(context);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setRobotoTypeface(context);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setRobotoTypeface(context);
    }

    private void setRobotoTypeface(Context context) {
        if (!this.isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "roboto-light.ttf");
            setTypeface(tf);
        }
    }
}
