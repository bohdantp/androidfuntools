package bodosoft.funtools.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import bodosoft.funtools.R;

/**
 * Created by bohdantp on 4/20/16.
 */
public class RecyclerSelector {

    private static final String TAG = "RecyclerSelector";

    private boolean inACtionMode = false;
    private AppCompatActivity activity;
    private ActionModeListener listener;
    private SparseArray<Boolean> selected;
    private ActionMode currentActionMode;

    public RecyclerSelector(AppCompatActivity activity, ActionModeListener listener) {
        this.activity = activity;
        this.listener = listener;
        selected = new SparseArray<>();
    }

    public void startSelectionMode() {
        activity.startSupportActionMode(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                currentActionMode = mode;
                inACtionMode = true;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                if (listener != null) {
                    listener.prepareActionModeMenu(mode, menu);
                }
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (listener != null) {
                    return listener.onActionItemClicked(mode, item);
                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                destrActMode();
            }
        });
    }

    private void destrActMode() {
        inACtionMode = false;
        currentActionMode = null;
        selected.clear();
        if (listener != null) {
            listener.onDestroyActionMode();
        }
    }

    public boolean isInActionMode() {
        return inACtionMode;
    }

    public void setSelected(int pos, boolean value) {
        selected.put(pos, value);
        refreshItemsTitle();
    }

    public void setSelectedReverse(int pos) {
        Boolean val = selected.get(pos, false);
        selected.put(pos, !val);
        refreshItemsTitle();
    }

    private void refreshItemsTitle() {
        if (currentActionMode != null) {
            int c = 0;
            for (int i = 0; i < selected.size(); i++) {
                int key = selected.keyAt(i);
                Boolean v = selected.get(key);
                if (v) {
                    c++;
                }
            }
            if (c < 1) {
                cancel();
            } else {
                currentActionMode.setTitle(activity.getString(R.string.selecteditems) + ": " + c);
            }
        }
    }

    public boolean isSelected(int pos) {
        return selected.get(pos, false);
    }

    public void cancel() {
        if (currentActionMode != null) {
            currentActionMode.finish();
        }
    }

    public List<Integer> getSelectedPositions() {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < selected.size(); i++) {
            int key = selected.keyAt(i);
            Boolean v = selected.get(key);
            if (v) {
                res.add(key);
            }
        }
        return res;
    }

    public interface ActionModeListener {
        void onDestroyActionMode();

        boolean onActionItemClicked(ActionMode mode, MenuItem item);

        void prepareActionModeMenu(ActionMode mode, Menu menu);
    }

}

