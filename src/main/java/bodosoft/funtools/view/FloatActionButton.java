package bodosoft.funtools.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageButton;

import bodosoft.funtools.R;
import bodosoft.funtools.view.tick.TickPlusDrawable;


public class FloatActionButton extends ImageButton {
    TickPlusDrawable d;

    public FloatActionButton(Context context) {
        super(context);
        init(context, null);
    }

    public FloatActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FloatActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private Drawable createButton(int bgcolor, int color) {
        d = new TickPlusDrawable(10, bgcolor, color);
        return d;
    }

    public void setBackColor(int color) {
        d.setBackColor(color);
    }

    public void toogle(TickPlusDrawable.Mode mode) {
        d.animateToMode(mode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void init(Context context, AttributeSet attrSet) {
        TypedArray a = context.obtainStyledAttributes(attrSet, R.styleable.floatActionButtonMy, 0, 0);
        int color = a.getColor(R.styleable.floatActionButtonMy_tick_color, Color.WHITE);
        int bgcolor = a.getColor(R.styleable.floatActionButtonMy_bg_color, Color.RED);
        try {
            Drawable button = createButton(bgcolor, color);
            setBackground(button);
        } catch (Throwable t) {
        } finally {
            a.recycle();
        }

    }

}
