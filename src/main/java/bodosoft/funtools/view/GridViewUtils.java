package bodosoft.funtools.view;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by bohdantp on 24.04.2014.
 */
public class GridViewUtils {


    public static int getNormalizedItemSize(Context context, int gridItemDimenResource, int gridSpacingDimenResource) {
        float dimension = context.getResources().getDimension(gridItemDimenResource);
        float spacing = context.getResources().getDimension(gridSpacingDimenResource);
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int dim = (int) dimension;
        int itemCount = width / (dim + (int) spacing);
        int delta = (width - (itemCount * dim) - (int) ((itemCount - 1) * spacing)) / itemCount;
        dim += delta;
        return dim;
    }

}
