package bodosoft.funtools.view.onetouch;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import bodosoft.funtools.R;
import bodosoft.funtools.animator.UniversalAnimatorListener;
import bodosoft.funtools.animator.ValueAnimator;
import bodosoft.funtools.utils.MapUtils;

/**
 * Created by bohdantp on 11/27/14.
 */
public class OneTouchProgressBar extends View {

    private static final int HINT_COLOR = 0X20000000;
    private static final int ICON_COLOR = 0X20000000;
    private static final int DEFAULT_PROGRESS_MAXIMIZE_ANIMATION_DURATION = 200;
    private static final int DEFAULT_PROGRESS_SEC_COLOR = 0x10000000;
    private static final int DEFAULT_PROGRESS_COLOR = 0x70000000;
    private static final int DEFAULT_PADDING_DP = 10;
    private static final int DEFAULT_ICONSIZE_DP = 50;
    private static final int DEFAULT_PROGRESSCIRCLEWIDTH_DP = 10;
    private static final int DEFAULT_MINPROGRESSRADIUS_DP = 75;
    private static final int DEFAULT_MAXPROGRESSDELTA_DP = 150;
    private static final int DEFAULT_MINDISTANCEFROMCENTER_DP = 115;
    private static final int DEFAULT_ICONHITMARGIN_DP = 10;
    private static final int DISTANCE_BETWEEN_CIRCLE_AND_HINT = 30;
    private static final String TAG = "OneTouchProgressBar";

    private int minDistanceFromCenter;
    private int iconHitMargin;
    private ObjectAnimator previousCircleEnlargeAnimator = null;
    private float padding;
    private float iconSize;
    private float progressCircleWidth;
    private float minProgressRadius;
    private int maximizeAnimationDuration;
    private float maxProgressDelta;
    private ValueAnimator progressCircleValueAnimator = new ValueAnimator();
    private boolean iconHitted = false;
    private float currentProgress = 0;
    private Gravity gravity = Gravity.RIGHT;
    private Listener listener;
    private int secondaryProgressColor;
    private int progressColor;

    public OneTouchProgressBar(Context context) {
        super(context);
        init(context, null);
    }

    public OneTouchProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public OneTouchProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void init(Context context, AttributeSet attrs) {


        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.OneTouchProgressBar, 0, 0);
        try {
            int anInt = a.getInt(R.styleable.OneTouchProgressBar_iconsGravity, 0);
            secondaryProgressColor = a.getColor(R.styleable.OneTouchProgressBar_second_color, DEFAULT_PROGRESS_SEC_COLOR);
            progressColor = a.getColor(R.styleable.OneTouchProgressBar_progress_color, DEFAULT_PROGRESS_COLOR);
            maximizeAnimationDuration = a.getInteger(R.styleable.OneTouchProgressBar_anim_duration, DEFAULT_PROGRESS_MAXIMIZE_ANIMATION_DURATION);
            padding = a.getDimensionPixelSize(R.styleable.OneTouchProgressBar_icon_padding, dpToPx(DEFAULT_PADDING_DP));
            iconSize = a.getDimensionPixelSize(R.styleable.OneTouchProgressBar_icon_size, dpToPx(DEFAULT_ICONSIZE_DP));
            minProgressRadius = a.getDimensionPixelSize(R.styleable.OneTouchProgressBar_min_progress_radius, dpToPx(DEFAULT_MINPROGRESSRADIUS_DP));
            maxProgressDelta = a.getDimensionPixelSize(R.styleable.OneTouchProgressBar_max_progress_radius_delta, dpToPx(DEFAULT_MAXPROGRESSDELTA_DP));
            minDistanceFromCenter = a.getDimensionPixelSize(R.styleable.OneTouchProgressBar_hint_radius, dpToPx(DEFAULT_MINDISTANCEFROMCENTER_DP));
            iconHitMargin = a.getDimensionPixelSize(R.styleable.OneTouchProgressBar_icon_hit_margin, dpToPx(DEFAULT_ICONHITMARGIN_DP));
            progressCircleWidth = a.getDimensionPixelSize(R.styleable.OneTouchProgressBar_circle_line_width, dpToPx(DEFAULT_PROGRESSCIRCLEWIDTH_DP));

            if (anInt == 0) {
                gravity = Gravity.LEFT;
            } else {
                gravity = Gravity.RIGHT;
            }
        } finally {
            a.recycle();
        }
        StringBuilder b = new StringBuilder("");
        b.append("iconsize: " + iconSize);
        b.append(" ");
        b.append("minDistanceFromCenter: " + minDistanceFromCenter);
        b.append(" ");
        Log.v(TAG, "" + b.toString());
    }

    private void setPadding(Canvas canvas, float padding) {
        float tx = 0;
        float ty = 0;
        ty = -padding;
        if (gravity == Gravity.LEFT) {
            tx = padding;
        }
        if (gravity == Gravity.RIGHT) {
            tx = -padding;
        }
        canvas.translate(tx, ty);
    }

    private void initialTranslate(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        int tx = 0, ty = height;
        if (gravity == Gravity.RIGHT) {
            tx = width;
        }
        canvas.translate(tx, ty);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        initialTranslate(canvas);
        setPadding(canvas, padding);
        drawIconHandle(canvas);
        drawHint(canvas);
        drawProgress(canvas);
        canvas.restore();
    }

    private void drawHint(Canvas canvas) {
        float progressRadius = getProgressRadius();
        float strokeWidth = progressRadius - minDistanceFromCenter;
        if (strokeWidth > 0) {
            float width = progressRadius - strokeWidth / 2 - DISTANCE_BETWEEN_CIRCLE_AND_HINT;
            float height = width;
            RectF rect = new RectF(-width, -height, width, height);
            Paint p = new Paint();
            setAliasing(p);
            p.setStyle(Paint.Style.STROKE);
            p.setStrokeWidth(strokeWidth);
//            p.setShader(new RadialGradient());
            p.setColor(HINT_COLOR);
            float start = 270;
            if (gravity == Gravity.RIGHT) {
                start = 180;
            }
            canvas.drawArc(rect, start, 90, false, p);
        }
    }

    private void drawIconHandle(Canvas canvas) {
        Paint p = new Paint();
        setAliasing(p);
        p.setColor(ICON_COLOR);
        RectF r = new RectF(0, 0, iconSize, iconSize);
        r.offset(0, -iconSize);
        if (gravity == Gravity.RIGHT) {
            r.offset(-iconSize, 0);
        }
        canvas.drawArc(r, 0, 360, true, p);
    }

    private RectF getIconRect() {
        int width = getWidth();
        int height = getHeight();
        float left = 0;
        if (gravity == Gravity.LEFT) {
            left = padding;
        }
        if (gravity == Gravity.RIGHT) {
            left = width - padding - iconSize;
        }
        float top = height - padding - iconSize;
        RectF res = new RectF(left, top, left + iconSize, top + iconSize);
        return res;
    }

    private boolean hitIcon(MotionEvent event) {
        RectF r = getIconRect();
        int m = iconHitMargin;
        r.set(r.left - m, r.top - m, r.right + m, r.bottom + m);
        return r.contains(event.getX(), event.getY());
    }

    private float getProgressRadius() {
        return progressCircleValueAnimator.getProgress() * maxProgressDelta + minProgressRadius;
    }

    private void setAliasing(Paint p) {
        p.setFlags(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG);
    }

    private void setRoundedLine(Paint p) {
        p.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
        p.setStrokeCap(Paint.Cap.ROUND);      // set the paint cap to round too
        p.setPathEffect(new CornerPathEffect(10));   // set the path effect when they join.
        p.setAntiAlias(true);
    }

    private void drawProgress(Canvas canvas) {
        float width = getProgressRadius();
        float height = getProgressRadius();
        RectF rect = new RectF(-width, -height, width, height);
        Paint p = new Paint();
        setAliasing(p);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeWidth(progressCircleWidth);
        setRoundedLine(p);
        p.setColor(secondaryProgressColor);
        Paint progress = new Paint();
        setAliasing(progress);
        progress.setStyle(Paint.Style.STROKE);
        setRoundedLine(progress);
        progress.setStrokeWidth(progressCircleWidth);
        progress.setColor(progressColor);
        float sweepAngle = 90 * currentProgress;
        if (gravity == Gravity.LEFT) {
            canvas.drawArc(rect, 270, sweepAngle, false, progress);
            canvas.drawArc(rect, 270 + sweepAngle, 90 - sweepAngle, false, p);
        }
        if (gravity == Gravity.RIGHT) {
            canvas.drawArc(rect, 180 + 90 - sweepAngle, sweepAngle, false, progress);
            canvas.drawArc(rect, 180, 90 - sweepAngle, false, p);
        }
    }

    private void updateProgress(MotionEvent event) {
        int height = getHeight();
        RectF iconRect = getIconRect();
        float x1 = iconRect.left;
        if (gravity == Gravity.RIGHT) {
            x1 = iconRect.right;
        }
        float y1 = height - iconRect.bottom;
        float x2 = event.getX();
        float y2 = height - event.getY();
        double distanceFromCenter = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        if (distanceFromCenter < minDistanceFromCenter) {
            return;
        }
        double atan = Math.atan2(y2 - y1, x2 - x1);
        double alfa = Math.toDegrees(atan);
        alfa = transpAngle(alfa);
        if (alfa > 180 && alfa < 270) {
            alfa = 180;
        }
        if (alfa > 270 && alfa < 360) {
            alfa = 0;
        }
        float progress = 0;
        if (gravity == Gravity.LEFT) {
            progress = MapUtils.map((float) alfa, 3f, 87f, 1f, 0f);
        }
        if (gravity == Gravity.RIGHT) {
            progress = MapUtils.map((float) alfa, 93f, 177, 0f, 1f);
        }
        setProgressInternall(progress, true);
    }

    private double transpAngle(double angle) {
        double res = angle;
        res += 360;
        if (res >= 360) {
            res -= 360;
        }
        return res;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean res = false;
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (hitIcon(event)) {
                    res = true;
                    iconHitted = true;
                    animateProgress(true);
                    if (listener != null) {
                        listener.onDragStart(this);
                    }
                }
                break;
            case MotionEvent.ACTION_MOVE:
                updateProgress(event);
                res = true;
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (iconHitted) {
                    animateProgress(false);
                    if (listener != null) {
                        listener.onDragEnd(this);
                    }
                }
                iconHitted = false;
                break;
        }
        return res;
    }


    private void setProgressInternall(float progress, boolean fromUser) {
        currentProgress = progress;
        if (listener != null) {
            listener.onProgressChange(currentProgress, this, fromUser);
        }
        invalidate();
    }

    private void animateProgress(boolean maximize) {
        ObjectAnimator animator = null;
        if (maximize) {
            float start = 0f;
            if (previousCircleEnlargeAnimator != null) {
                if (previousCircleEnlargeAnimator.isRunning()) {
                    previousCircleEnlargeAnimator.cancel();
                    start = progressCircleValueAnimator.getProgress();
                }
            }
            animator = ObjectAnimator.ofFloat(progressCircleValueAnimator, "progress", start, 1f);
        } else {
            if (previousCircleEnlargeAnimator != null) {
                if (previousCircleEnlargeAnimator.isRunning()) {
                    previousCircleEnlargeAnimator.cancel();
                }
            }
            animator = ObjectAnimator.ofFloat(progressCircleValueAnimator, "progress", progressCircleValueAnimator.getProgress(), 0);
        }
        UniversalAnimatorListener listener = new UniversalAnimatorListener() {
            @Override
            public void onAnimationUpdate() {
                invalidate();
            }

            @Override
            public void onAnimationEnd() {

            }
        };
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addListener(listener);
        animator.addUpdateListener(listener);
        animator.setDuration(maximizeAnimationDuration);
        previousCircleEnlargeAnimator = animator;
        animator.start();
    }

    public void setProgress(float progress) {
        if (progress < 0) {
            progress = 0;
        }
        if (progress > 1) {
            progress = 1;
        }

        setProgressInternall(progress, false);
    }

    public enum Gravity {
        LEFT, RIGHT;
    }

    public static interface Listener {
        void onDragStart(OneTouchProgressBar progressBar);

        void onProgressChange(float progress, OneTouchProgressBar progressBar, boolean fromUser);

        void onDragEnd(OneTouchProgressBar progressBar);
    }

}
