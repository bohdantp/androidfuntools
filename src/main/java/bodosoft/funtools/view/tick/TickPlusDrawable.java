package bodosoft.funtools.view.tick;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Property;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import bodosoft.funtools.view.tick.vertex.AnimatedVertex;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class TickPlusDrawable extends Drawable {

    private static final long ANIMATION_DURATION = 500;
    private static final Interpolator ANIMATION_INTERPOLATOR = new DecelerateInterpolator();
    private static int red = 0xFFF44336;
    private static int yellow = 0xFFFFFF00;
    private static int green = 0xFF76FF03;
    private final RectF signBounds = new RectF();
    private final AnimatedVertex.UpdateListener listener = new AnimatedVertex.UpdateListener() {
        @Override
        public void refresh() {
            invalidateSelf();
        }
    };
    float mRotation = 0;
    private Paint mLinePaint;
    private Paint mBackgroundPaint;
    private ArgbEvaluator mArgbEvaluator = new ArgbEvaluator();
    private int mStrokeWidth = 10;
    private int mTickColor = Color.RED;
    private int mPlusColor = Color.WHITE;
    private Property<TickPlusDrawable, Integer> mBackgroundColorProperty = new Property<TickPlusDrawable, Integer>(Integer.class, "bg_color") {
        @Override
        public Integer get(TickPlusDrawable object) {
            return object.mBackgroundPaint.getColor();
        }

        @Override
        public void set(TickPlusDrawable object, Integer value) {
            object.mBackgroundPaint.setColor(value);
        }
    };
    private Property<TickPlusDrawable, Integer> mLineColorProperty = new Property<TickPlusDrawable, Integer>(Integer.class, "line_color") {
        @Override
        public Integer get(TickPlusDrawable object) {
            return object.mLinePaint.getColor();
        }

        @Override
        public void set(TickPlusDrawable object, Integer value) {
            object.mLinePaint.setColor(value);
        }
    };
    private AnimatedVertex D1, D2, D3, D4, D5, D6, D7, D8, D9;
    private AnimatedVertex A, B, C, D, E, F;
    private Mode mode = Mode.PLUS;
    private boolean setuped = false;
    private Property<TickPlusDrawable, Float> mRotationProperty = new Property<TickPlusDrawable, Float>(Float.class, "rotation") {
        @Override
        public Float get(TickPlusDrawable object) {
            return object.mRotation;
        }

        @Override
        public void set(TickPlusDrawable object, Float value) {
            object.mRotation = value;
        }
    };

    public TickPlusDrawable(int strokeWidth, int tickColor, int plusColor) {
        mStrokeWidth = strokeWidth;
        mTickColor = tickColor;
        mPlusColor = plusColor;
        setupPaints();
    }

    private void setupPaints() {
        mLinePaint = new Paint(ANTI_ALIAS_FLAG);
        mLinePaint.setStyle(Paint.Style.STROKE);
        mLinePaint.setColor(mPlusColor);
        mLinePaint.setStrokeWidth(mStrokeWidth);
        mLinePaint.setStrokeCap(Paint.Cap.ROUND);
        mBackgroundPaint = new Paint(ANTI_ALIAS_FLAG);
        mBackgroundPaint.setStyle(Paint.Style.FILL);
        mBackgroundPaint.setColor(mTickColor);
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        int padding = bounds.centerX() / 2;
        signBounds.left = bounds.left + padding;
        signBounds.right = bounds.right - padding;
        signBounds.top = bounds.top + padding;
        signBounds.bottom = bounds.bottom - padding;
        setupConstDots();
    }

    private void setupConstDots() {
        D1 = new AnimatedVertex(signBounds.left, signBounds.centerY(), "D1", null);
        D2 = new AnimatedVertex(signBounds.right, signBounds.centerY(), "D2", null);
        D3 = new AnimatedVertex(signBounds.centerX(), signBounds.top, "D3", null);
        D4 = new AnimatedVertex(signBounds.centerX(), signBounds.bottom, "D4", null);
        D5 = new AnimatedVertex(signBounds.centerX(), signBounds.centerY(), "D5", null);
        D6 = new AnimatedVertex(signBounds.right, signBounds.top, "D6", null);
        D7 = new AnimatedVertex(signBounds.centerX() + signBounds.width() / 4, signBounds.centerY(), "D7", null);
        D8 = new AnimatedVertex(signBounds.centerX() + signBounds.width() / 4, signBounds.centerY() - signBounds.height() / 4, "D8", null);
        D9 = new AnimatedVertex(signBounds.centerX() + signBounds.width() / 4, signBounds.centerY() + signBounds.height() / 4, "D9", null);
        A = D1.getCloned("A", listener);
        B = D2.getCloned("B", listener);
        C = D3.getCloned("C", listener);
        D = D5.getCloned("D", listener);
        E = D4.getCloned("E", listener);
        F = D5.getCloned("F", listener);
        setuped = true;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(signBounds.centerX(), signBounds.centerY(), signBounds.centerX(), mBackgroundPaint);
        canvas.save();
        canvas.rotate(360 * mRotation, getBounds().width() / 2, getBounds().height() / 2);
        if (setuped) {
            drawLine(canvas, A, B, mLinePaint);
            drawLine(canvas, C, D, mLinePaint);
            drawLine(canvas, E, F, mLinePaint);
        }
        canvas.restore();
    }

    public void drawLine(Canvas c, AnimatedVertex a, AnimatedVertex b, Paint p) {
        c.drawLine(a.getX(), a.getY(), b.getX(), b.getY(), p);
    }

    public void animateToMode(Mode mode) {
        if (setuped) {
            switch (mode) {
                case PLUS:
                    animateToPlus();
                    break;
                case STRELKA:
                    animateToStrelka();
                    break;
                case TICK:
                    animateToTick();
                    break;
            }
        }
    }

    private void animateToStrelka() {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
//                ObjectAnimator.ofObject(this, mBackgroundColorProperty, mArgbEvaluator, yellow),
                ObjectAnimator.ofObject(this, mLineColorProperty, mArgbEvaluator, mPlusColor),
                ObjectAnimator.ofFloat(this, mRotationProperty, 0, 1)

        );
        A.animateTo(set, D1);
        B.animateTo(set, D2);
        C.animateTo(set, D8);
        D.animateTo(set, D2);
        E.animateTo(set, D9);
        F.animateTo(set, D2);

        set.setDuration(ANIMATION_DURATION);
        set.setInterpolator(ANIMATION_INTERPOLATOR);
        set.start();
        mode = Mode.STRELKA;
    }

    private void animateToTick() {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
//                ObjectAnimator.ofObject(this, mBackgroundColorProperty, mArgbEvaluator, green),
                ObjectAnimator.ofObject(this, mLineColorProperty, mArgbEvaluator, mPlusColor),
                ObjectAnimator.ofFloat(this, mRotationProperty, 0, 1)
        );
        A.animateTo(set, D1);
        B.animateTo(set, D4);
        C.animateTo(set, D6);
        E.animateTo(set, D4);
        D.animateTo(set, D7);
        F.animateTo(set, D7);

        set.setDuration(ANIMATION_DURATION);
        set.setInterpolator(ANIMATION_INTERPOLATOR);
        set.start();
        mode = Mode.TICK;
    }

    private void animateToPlus() {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
//                ObjectAnimator.ofObject(this, mBackgroundColorProperty, mArgbEvaluator, red),
                ObjectAnimator.ofObject(this, mLineColorProperty, mArgbEvaluator, mPlusColor),
                ObjectAnimator.ofFloat(this, mRotationProperty, 0, 1)
        );
        A.animateTo(set, D1);
        B.animateTo(set, D2);
        C.animateTo(set, D3);
        D.animateTo(set, D5);
        E.animateTo(set, D4);
        F.animateTo(set, D5);
        set.setDuration(ANIMATION_DURATION);
        set.setInterpolator(ANIMATION_INTERPOLATOR);
        set.start();
        mode = Mode.PLUS;
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public void setBackColor(int color) {
        mTickColor = color;
        setupPaints();
        invalidateSelf();
    }

    public enum Mode {
        PLUS, STRELKA, TICK
    }

}
