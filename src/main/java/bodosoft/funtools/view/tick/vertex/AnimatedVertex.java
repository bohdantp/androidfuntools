package bodosoft.funtools.view.tick.vertex;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.util.Property;

/**
 * Created by bohdantp on 5/20/15.
 */
public class AnimatedVertex {

    private final String vertexName;
    private final UpdateListener updateListener;
    private float x;
    private float y;
    private XPointProperty xprop;
    private YPointProperty yprop;

    public AnimatedVertex(float x, float y, String vertexName, UpdateListener listener) {
        this.x = x;
        this.y = y;
        this.vertexName = vertexName;
        this.updateListener = listener;
        this.xprop = new XPointProperty(vertexName);
        this.yprop = new YPointProperty(vertexName);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public AnimatedVertex getCloned(String name, UpdateListener listener) {
        return new AnimatedVertex(x, y, name, listener);
    }

    public void animateTo(AnimatorSet set, AnimatedVertex vertex) {
        set.playTogether(
                ObjectAnimator.ofFloat(this, xprop, vertex.x),
                ObjectAnimator.ofFloat(this, yprop, vertex.y)
        );
    }

    public interface UpdateListener {
        void refresh();
    }

    private abstract class PointProperty extends Property<AnimatedVertex, Float> {

        private PointProperty(String vertexName) {
            super(Float.class, vertexName);
        }
    }

    private class XPointProperty extends PointProperty {

        private XPointProperty(String vertexName) {
            super(vertexName);
        }

        @Override
        public Float get(AnimatedVertex object) {
            return object.x;
        }

        @Override
        public void set(AnimatedVertex object, Float value) {
            object.x = value;
            updateListener.refresh();
        }
    }

    private class YPointProperty extends PointProperty {

        private YPointProperty(String vertexName) {
            super(vertexName);
        }

        @Override
        public Float get(AnimatedVertex object) {
            return object.y;
        }

        @Override
        public void set(AnimatedVertex object, Float value) {
            object.y = value;
            updateListener.refresh();
        }
    }

}
