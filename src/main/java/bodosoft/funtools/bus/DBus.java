package bodosoft.funtools.bus;

import android.os.Bundle;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by bohdantrofymchuk on 1/21/16.
 */
public class DBus {

    private HashMap<String, Set<Observer>> observers;

    public DBus() {
        observers = new HashMap<String, Set<Observer>>();
    }

    public boolean sendEvent(String eventName, Bundle data) {
        boolean res = false;
        Set<Observer> arr = observers.get(eventName);
        if (arr != null) {
            for (Observer o: arr){
                res = o.observeEvent(eventName, data) | res;
            }
        }
        return res;
    }

    public void register(String eventName, Observer observer) {
        Set<Observer> arr = observers.get(eventName);
        if (arr != null) {
            arr.add(observer);
        } else {
            arr = new HashSet<Observer>();
            arr.add(observer);
            observers.put(eventName, arr);
        }
    }

    public void unregister(String eventName, Observer observer) {
        Set<Observer> arr = observers.get(eventName);
        if (arr != null) {
            arr.remove(observer);
        }
    }

    public static interface Observer {
        boolean observeEvent(String eventName, Bundle data);
    }

}
