package bodosoft.funtools.loader;

/**
 * Created by bohdantp on 08.08.2014.
 */
public interface LoaderSupportAdapterInterface<P> {
    void onLoadedNewPart(P p);
}
