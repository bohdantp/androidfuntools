package bodosoft.funtools.loader;

import android.os.Bundle;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class ContentLoader<P> {

    private ExecutorService exec = Executors.newFixedThreadPool(1);
    private CallBack<P> callBack;
    private State state = State.IDLE;
    private Runnable loader = new Runnable() {
        public void run() {
            try {
                P p = loadNextPart();
                callBack.onLoaded(p);
            } catch (Exception e) {
                callBack.onLoadError(e.getMessage());
            }
            state = State.IDLE;
        }
    };
    private Runnable loaderCount = new Runnable() {
        public void run() {
            try {
                P p = loadNextPart();
                callBack.onLoaded(p);
            } catch (Exception e) {
                callBack.onLoadError(e.getMessage());
            }
        }
    };

    public ContentLoader() {
    }

    public void restoreState(Bundle opts) {

    }

    public void setCallBack(CallBack<P> callBack) {
        this.callBack = callBack;
    }

    public State getState() {
        return state;
    }

    protected abstract P loadNextPart() throws Exception;

    public P loadNextSync() {
        if (state != State.IDLE) {
            throw new IllegalStateException("Already loading");
        }
        try {
            return loadNextPart();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void loadNext() {
        if (state != State.IDLE) {
            throw new IllegalStateException("Already loading");
        }
        state = State.LOADING;
        exec.execute(loader);
    }

    public void loadNext(int count) {
        if (count < 1) {
            return;
        }
        if (state != State.IDLE) {
            throw new IllegalStateException("Already loading");
        }
        state = State.LOADING;
        for (int i = 0; i < count - 1; i++) {
            exec.execute(loaderCount);
        }
        exec.execute(loader);
    }

    public enum State {
        IDLE, LOADING;
    }


    public interface CallBack<P> {
        void onLoaded(P p);

        void onLoadError(String errMsg);
    }

}
