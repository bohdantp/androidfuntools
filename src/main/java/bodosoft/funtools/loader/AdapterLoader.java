package bodosoft.funtools.loader;

public class AdapterLoader<P> {

    public static enum State {
        IDLE, WORKING, DISPOSED;
    }

    public static interface LoadCallback<P> {
        public void loadFinished(P p);

        public void loadError();
    }

    private LoaderSupportAdapterInterface<P> adapter;
    private ContentLoader<P> loader;
    private volatile State state = State.IDLE;
    private Object syncObject = new Object();
    private LoadCallback<P> callback;

    public AdapterLoader(LoaderSupportAdapterInterface<P> adapter, ContentLoader<P> loader) {
        this.adapter = adapter;
        this.loader = loader;
        this.loader.setCallBack(callBack);
    }

    public void loadNext() {
        if (state != State.WORKING) {
            if (loader.getState() == ContentLoader.State.IDLE) {
                state = State.WORKING;
                loader.loadNext();
            }
        }
    }

    public void loadNext(LoadCallback<P> callback) {
        synchronized (syncObject) {
            this.callback = callback;
            loadNext();
        }
    }


    public void loadNext(int count) {
        if (state != State.WORKING) {
            if (loader.getState() == ContentLoader.State.IDLE) {
                state = State.WORKING;
                loader.loadNext(count);
            }
        }
    }

    private ContentLoader.CallBack callBack = new ContentLoader.CallBack<P>() {

        @Override
        public void onLoaded(P p) {
            synchronized (syncObject) {
                state = State.IDLE;
                adapter.onLoadedNewPart(p);
                if (callback != null) {
                    callback.loadFinished(p);
                    callback = null;
                }
            }
        }

        @Override
        public void onLoadError(String errMsg) {
            synchronized (syncObject) {
                state = State.IDLE;
                if (callback != null) {
                    callback.loadError();
                    callback = null;
                }
            }
        }

    };

    public void dispose() {
        state = State.DISPOSED;
        loader = null;
        adapter = null;
        callBack = null;
    }
}
